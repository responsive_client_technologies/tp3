**TP Responsive Web Design**
----------------------------

![](img/exemple.gif)

1. Installer Bootstrap

2. Centrer les �l�ments sur la page grace � un container Bootstrap

3. Utiliser la grille fluide de Bootstrap pour d�finir la largeur des �l�ments

4. Utiliser les classes de grille (col-xs-* / col-md-* etc.) pour afficher successivement les albums par 6 sur grand �cran, 4 pour les tablettes et 3 en dessous

5. Utiliser Bootstrap pour :
	- Masquer la colonne de gauche sur mobile et sur tablette en mode portrait
	- Aligner les liens Collection et History � droite lorsque l'espace disponible devient insuffisant
	- Mettre en place le menu d�roulant (en haut � droite)
	- Afficher les ic�nes dans le menu d�roulant

6. Remplacer toutes les tailles de police en pixel par des unit�s relatives (em ou rem)

7. R�gler le viewport pour les mobiles

8. Utiliser les medias queries pour :
	- Masquer le champs de recherche lorsque l'espace disponible devient insuffisant
	- Masquer les ic�nes twitter et facebook sur mobile
	- Pr�senter les albums 2 par 2 � partir d'une largeur d'�cran de 450px

9. Ajuster la taille du texte aux 3 points de ruptures suivants : 1400px (grossir) / 600px (plus petit) / 450px (encore plus petit)

10. Redimensionner la fen�tre du navigateur, et tester que la page est visuellement coh�rente � toutes les r�solutions. Faire les ajustements n�cessaires avec les media queries